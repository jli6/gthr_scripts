# gthr_scripts

Scripts to use several APIs.
If you've stumbled upon this repo by chance, chances are it won't be of much 
use to you. The code in this repo is adapted from Spotipy 
(https://github.com/plamere/spotipy) and YouTube API Samples 
(https://github.com/youtube/api-samples).

In most of the scripts in this repository you can find helpful comments, mostly at 
the top of the file (after the import statements).

## Prerequisites
I run all the scripts with Python 3. I suggest at least Python 3.5. You'll also need pip to install python packages.

These scripts use different online services and each one has different requirements.
Please note that we have existing accounts you can use if you want.

For scripts that use ACRCloud, you will need:
* an ACRCloud account with a "project", host, access key, and access secret

   Store your ACRCloud project credentials (host, access key, and access secret)
   in a file called projectinfo.txt, located in ./data/credentials/acrcloud/.
   This file should adhere to the following format: host on the first line, access key on the second line, 
   access secret on the third line. I have provided a template, projectinfo_example.txt
   (also located in ./data/credentials/acrcloud), that shows you what projectinfo.txt 
   should look like. Don't use this template file directly as its credentials are invalid.
   
* the ACRCloud SDK

   The ACRCloud SDK for Python is located at https://github.com/acrcloud/acrcloud_sdk_python.
   It can be installed via the following command:
   ```$ python3 -m pip install git+https://github.com/acrcloud/acrcloud_sdk_python```
   Note if you're a Windows user you will have to install vcredist first, as indicated by the ACRCloud SDK for Python GitHub page.
   
For scripts that use Spotify, you will need:
* a Spotify account with an "app", client ID and client secret

   Store your Spotify app credentials (client ID and client secret) in a file called 
   client_credentials.txt, located in ./data/credentials/spotify. This file should 
   adhere to the following format: client ID on the  first line, client secret on
   the second line. An example of what this looks like is available in 
   client_credentials_template.txt, also located in ./data/credentials/spotify.
   
* the Spotipy library

   The Spotipy library can be installed via the following command:
   ```$ pip3 install spotipy --user```

For scripts that use YouTube, you will need:
* a Google account with the YouTube Data API and YouTube Analytics API enabled, as well as corresponding API keys

   Download your YouTube Analytics API credentials as a JSON file from Google,
   and store it in ./data/credentials/youtube. Save your YouTube Data API developer key
   into a file called developer_key, located in ./data/credentials/youtube.
   
*   Python 2.6 or greater
*   The pip package management tool
*   google-api-python-client
*   google-auth
*   google-auth-oauthlib
*   google-auth-httplib2
* anything else indicated in https://github.com/youtube/api-samples/blob/master/python/README.md

## My development environment 
I've successfully run these scripts with Python 3.5.2 (Anaconda distribution) in the terminal on Linux 64-bit.
I use PyCharm for editing and at least some of these scripts should work when run within PyCharm.
Since I haven't tested these on other platforms or environments, some errors or weird behavior might pop up when you run the code.
I don't expect you to replicate my environment! 

## Scripts
Some scripts are ready to run. Others have hidden bugs and/or are incomplete.
Scripts you will find the most useful, in alphabetical order:

* `acrcloud_recognize_wholevideo.py`
* `acrcloud_to_spotify_ids.py`
* `dl_spotify_from_youtube.py`
* `filter_spotify_playlist.py` (incomplete, but still very useful!)
* `spotify_audio_features_for_track.py`
* `spotify_start_end_times_playlist_to_file.py`
* `youtube_analytics_view_retention.py`
* `youtube_request_user_authorization.py`

Due to how relative filepaths work in Python, you will need to change your
current directory to ./src/ before running these scripts. I'd like to fix this
issue in the future.

### `acrcloud_recognize_wholevideo.py`
* What does this script do?
   * Detect music playing and when they're playing in an audio file, 
   using the ACRCloud service
* How do I run this?
   * Run with the `-h` option to view the help page.
* Example command:
   * `$ python3 acrcloud_recognize_wholevideo.py --shift 3 --duration 10 /path/to/audio/file.mp3` 
* Implementation details
   * Uses a hopping window method to analyze the entire audio file with custom user-specified time precision
   * Sends "many" API queries (e.g. a 3-minute video may take as many as 60-180 queries)

### `acrcloud_test.py`
* What does this script do?
   * Detect music playing during the first 12 seconds of an audio clip, 
   using the ACRCloud service
* How do I run this?
   * Run with one argument representing a path to an audio file
* Example command:
   * `$ python3 acrcloud_test.py /path/to/audio/file.ogg`
* Implementation details
   * Sends one query only. Useful for identifying a song in an audio file 
   consisting solely of a song excerpt or hummed melody.

### `acrcloud_test_timeargs.py`
* What does this script do?
   * Detect music playing during a user-specified time interval of an audio clip, 
   using the ACRCloud service
* How do I run this?
   * Run with three arguments in the following order:
      * a path to an audio file
      * start time of time interval, in seconds
      * duration of time interval, in seconds
* Example command:
   * `$ python3 acrcloud_test.py /path/to/audio/file.opus 65 12`
* Implementation details
   * Sends one query only. Useful for identifying a song you hear, but
   can't identify, in the middle of a video 

### `acrcloud_to_spotify_ids.py`
* What does this script do?
   * Indicates which Spotify tracks (indicated by track IDs) are present in an already-analyzed audio file 
* How do I run this?
   * Run with the `-h` option to view the help page.
   * You'll need to run `acrcloud_recognize_wholevideo.py` on the desired audio file 
   before running this script.
* Example command:
   * `$ python3 acrcloud_to_spotify_ids.py /path/to/json/file.json --threshold 100`
* Implementation details
   * No queries are performed. Rather, it interprets the responses of the ACRCloud queries.

### `dl_spotify_from_youtube.py`
* What does this script do?
   * Download a song indicated by its Spotify link or track ID
* How do I run this?
   * Run with the `-h` option to view the help page.
* Example command:
   * `$ python3 dl_spotify_from_youtube.py https://open.spotify.com/user/1nkmgmb9adiarj8t67yhuozxz/playlist/11wTcHdNxG4CDNd21D1W4N`
* Implementation details
   * Uses a heuristic approach to retrieve media from YouTube
   * Uses the YouTube API to perform search queries
   * Much more is available in Downloader_Reasoning

### `dl_spotify_from_youtube_web.py`
* What does this script do?
   * Nothing, it's incomplete
* How do I run this?
   * Don't bother
* Example command:
   * Not available
* Implementation details
   * Uses a heuristic approach to retrieve media from YouTube
   * Uses a web scraper to perform search queries
   * Much more is available in Downloader_Reasoning

### `filter_spotify_playlist.py`
* What does this script do?
   * Filter out songs recognized by an audio content recognition (ACR) service from a Spotify playlist.
* How do I run this?
   * Run with the `-h` option to view the help page.
* Example command:
   * `$ python3 https://open.spotify.com/user/1nkmgmb9adiarj8t67yhuozxz/playlist/02SgGnmOpwmaEYpMuu5aqo`
* Implementation details
   * Prints out results rather than creating a new filtered Spotify playlist 
   because we haven't implemented a callback server

### `liveme_get_video_info.js`
* What does this script do?
   * Retrieve information about a Liveme stream ("video")
* How do I run this?
   * Before running, install the unofficial-looking Liveme API node package.
   You can do so with the command `$ npm install git+https://git@github.com/Lewdninja/liveme-api.git`
   * Replace the dummy credentials in the code with your Liveme account credentials.
   This code only supports Liveme accounts that have been registered via email address and password 
   (as opposed to Google integration, for instance).
   * Replace the input string of getVideoInfo() with the ID of the video you want.
   * Then see the example command below.
* Example command:
   * `$ node liveme_get_video_info.js`
* Implementation details
   * None

### `pandora_track_mgp.py`
* What does this script do?
   * Retrieve Pandora "Music Genome Project" data for a track 
   using an unofficial Pandora API (there is no official one).
* How do I run this?
   * Before running, replace the dummy credentials in the code with your Pandora account's credentials.
   * See the example command below to run.
* Example command:
   * `$ python3 pandora_track_mgp.py`
* Implementation details
   * Requires a track token; I don't know how to get a track token for a custom track

### `spotify_audio_features_for_track.py`
* What does this script do?
   * Retrieve Spotify audio features for many (up to 50) user-specified tracks
* How do I run this?
   * Create a text file containing Spotify track IDs (or URLs) separated by newlines
   * Run with the `-h` option to view the help page.
* Example command:
   * `$ python3 spotify_audio_features_for_track.py /path/to/file_with_ids.txt /path/to/features_output.txt`
* Implementation details
   * None

### `spotify_audio_features_for_track_old.py`
* What does this script do?
* How do I run this?
* Example command:
* Implementation details

### `spotify_start_end_times_playlist_old.py`
* What does this script do?
* How do I run this?
* Example command:
* Implementation details

### `spotify_start_end_times_playlist_to_file.py`
* What does this script do?
   * Compute start and end times of songs in a Spotify playlist, 
   relative to the beginning of the playlist, as if the playlist was played
   from start to finish with a 10-second crossfade and no shuffle
* How do I run this?
   * Run with the `-h` option to view the help page.
* Example command:
   * `$ python3 spotify_start_end_times_playlist_to_file.py 
   https://open.spotify.com/user/1nkmgmb9adiarj8t67yhuozxz/playlist/11wTcHdNxG4CDNd21D1W4N 
   /path/to/file_times.csv`
* Implementation details
   * None

### `utils_acrcloud.py`
* What does this script do?
   * Nothing, it stores a bunch of functions used by other scripts

### `utils_acrcloud.py`
* What does this script do?
   * Nothing, it stores a bunch of functions used by other scripts

### `utils_youtube.py`
* What does this script do?
   * Nothing, it stores a bunch of functions used by other scripts

### `youtube_analytics_view_retention.py`
* What does this script do?
   * Get view retention data for a YouTube video
* How do I run this?
   * Run the `youtube_request_user_authorization.py` script beforehand so you
   have permissions to view a specific channel's analytics data
   * Run with the `-h` option to view the help page.
* Example command:
   * `$ python3 BaW_jenozKc --auth /path/to/pickle/file.pckl --plot`
* Implementation details

### `youtube_request_user_authorization.py`
* What does this script do?
   * Request a YouTube channel owner's permission to do certain things with their channel (i.e. read analytics data)
* How do I run this?
   * Run with the `-h` option to view the help page.
* Example command:
   * `$ python3 youtube_request_user_authorization.py --channel AnyStringGoesHere
* Implementation details
   * None