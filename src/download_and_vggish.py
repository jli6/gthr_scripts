from subprocess import call, PIPE, run
import argparse
from glob import glob

# This is a v0.0.1 level hackjob -- enough to say it works, but it's begging for improvement!

# uses subprocess, system-dependent
# works on linux mint
# probably works on other unix-like OSes, you will need python3 and find
# and you need to download models/research/audioset and put it in the proper place
# (in the parent directory of gthr_scripts)


def spotify_uri_to_track_id(uri):
    return uri.split('/')[-1]

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('track_uri', help='Spotify track URI')
    args = parser.parse_args()

    # Download audio
    call(["python3", "dl_spotify_from_youtube.py", args.track_uri])

    # Get filename of downloaded file
    file = glob("../data/yt_downloads/{id}*".format(id=spotify_uri_to_track_id(args.track_uri)))[0]
    file_minus_ext = file.split(sep=".", maxsplit=1)[0]
    print(file.split(sep=".", maxsplit=1))
    print(file_minus_ext)
    filewav = file_minus_ext + ".wav"

    # Convert downloaded file to wav
    call(["ffmpeg", "-i", file, filewav])

    # Convert wav audio to 128-dimensional embeddings
    call(["python3", "../../models/research/audioset/vggish_inference_demo.py",
          "--wav_file", filewav,
          "--tfrecord_file", "../data/binary/tf_record_file",
          "--checkpoint", "../../models/research/audioset/vggish_model.ckpt",
          "--pca_params", "../../models/research/audioset/vggish_pca_params.npz"])