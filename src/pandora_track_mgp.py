from pandora.clientbuilder import APIClientBuilder
import json

# This script retrieves Pandora's "Music Genome Project" information for a specified Pandora track.
# The Pandora API is unofficial, so this may not work in the future. The unofficial documentation is available at
# https://6xq.net/pandora-apidoc/
# This script most likely uses the JSON API v5 as opposed to the REST API.

# Before running, install the pydora package (you can do this through pip).
# HOW TO RUN:
# $ python3 pandora_track_mgp.py

client_fac = APIClientBuilder()

# Settings obtained from https://6xq.net/pandora-apidoc/json/partners/#partners
# Why are we using the android settings? Because they work!
# settings_dict_windows = {
#     "DECRYPTION_KEY": "U#IO$RZPAB%VX2",
#     "ENCRYPTION_KEY": "2%3WCL*JU$MP]4",
#     "PARTNER_USER": "pandora one",
#     "PARTNER_PASSWORD": "TVCKIBGS9AO9TSYLNNFUML0743LH82D",
#     "DEVICE": "D01",
# }
# settings_dict_vista = {
#     "DECRYPTION_KEY": "E#IO$MYZOAB%FVR2",
#     "ENCRYPTION_KEY": "%22CML*ZU$8YXP[1",
#     "PARTNER_USER": "windowsgadget",
#     "PARTNER_PASSWORD": "EVCCIBGS9AOJTSYMNNFUML07VLH8JYP0",
#     "DEVICE": "WG01",
# }
settings_dict_android = {
    "DECRYPTION_KEY": "R=U!LH$O2B#",
    "ENCRYPTION_KEY": "6#26FRL$ZWD",
    "PARTNER_USER": "android",
    "PARTNER_PASSWORD": "AC7IBG09A3DTSYM4R41UJWL07VLN8JI7",
    "DEVICE": "android-generic",
}
client = client_fac.build_from_settings_dict(settings_dict_android)

# TODO: replace this with your real email and password for a Pandora account.
# DON'T PUSH YOUR CODE IF IT HAS YOUR CREDENTIALS IN IT!!!
client.login("email address", "password")

# explain_track() is a wrapper for the API query: https://6xq.net/pandora-apidoc/json/play/#track-explaintrack
result = client.explain_track(
    # TODO: Replace the below string with the desired track's track token
    # However, it's challenging to find the track token of a desired track -- I don't know how to do so!
    "94f36e09e341780c2ee7ebbb3581a55c4f2066dbaa60f2ee253ede5bc407fbd3c4f6db7ed00f92312437e020e0bf0e05d2924742c2ccece2"
)

print(json.dumps(result, sort_keys=False, indent=4, separators=(',', ': ')))

# How to find a specific song's track id? I have no idea
# result2 = client.get_station_list()
# pl2 = client.get_playlist(list(result2.keys())[1])
# pl2.