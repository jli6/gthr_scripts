#!/usr/bin/python
#from __future__ import unicode_literals
import argparse

import utils_spotify
from googleapiclient.errors import HttpError

import utils_youtube


# This script uses the YouTube API to look at YouTube search results and link up a Spotify track with an acceptable
# YouTube video version. This script also interfaces with youtube-dl, a Python program that downloads media from
# websites.
# Note: If you use this script many times with the same Developer Key every time and/or from the same IP address(es),
# YouTube will probably be suspicious and possibly block you.
# Note 2: The scoring mechanism (i.e. how the program chooses the "best" search result) is a rough draft at this time
# and needs improvement! What it looks at is mostly sound, but the actual numbers need improving.
# TODO: Modify this script to work with MANY Spotify track IDs as input, like spotify/spotify_audio_features_for_track.py.
#

# TODO: Improve the scoring heurstic!
def score_video(video, video_duration, artists, track_name, track_duration, index):
    """

    :param video:
    :param artists:
    :param track_name:
    :return:
    """
    # scores a single video
    # needs improvement

    # if the query to youtube api for a search returns an array of youtube#searchResult,
    # then video would correspond to an entry of that array
    # e.g. results[2]

    # song name, before left parentheses or '-'

    # Scores
    integrity = 1
    correct_song = 1
    duration = 1

    # Shortcuts:
    channel_title = video['snippet']['channelTitle']
    video_title = video['snippet']['title']
    # video_duration

    # Good keywords:
    good_channel_title_keywords = {"Records", "Official"}
    good_video_title_keywords = {"Official", "Official Audio", "Audio", "Lyric Video"}
    good_video_description_keywords = {"Provided to YouTube by", "Official Audio", "Lyric Video"}

    # Bad keywords:
    bad_channel_title_keywords = {}
    bad_video_title_keywords = {"Music Video", "Interview", "Cover", "Live", "Video Clip"}.union({"Remix", "Edit", "Version"})
    bad_video_description_keywords = {}

    # Desirable keywords:
    # "Official", "Records", "Provided to YouTube by <record label>", "Official Audio", "Audio", "Lyric Video"?,
    # Possibly undesirable keywords:
    # "Video", "Interview", "Music Video", "Cover", "Remix", "Version", "Edit", "Live"
    # Desirable others:
    # "Stream/download here:"-style links in the description,
    # Possibly undesirable others:
    # Thumbnail images that look like they belong to a music video (would probably take a mini-AI of its own... lol)
    # Also consider the index of the search result. Lower results are less likely to be good...what good would a search engine be otherwise?

    # Source integrity
    if channel_title[-8:] == u' - Topic':  # Topic channels
        integrity += 1024
    elif any([artists[i].lower() == channel_title.lower() for i in range(len(artists))]):  # artist name IS channel title
        integrity += 512
    elif any([artists[i].lower() in channel_title.lower() for i in range(len(artists))]):  # artist name in channel title
        integrity += 256
    elif any([keyword.lower() in video_title.lower() for keyword in bad_video_title_keywords]):  # penalize SEVERELY if it's got a "bad keyword"
        integrity = 0  # music videos are better than nothing, and 1 is better than 0...
    else:  # don't add to integrity score
        integrity = 1

    # Making sure the song is the correct song
    if track_name.lower() in video_title.lower() and \
            any([artists[i].lower() in video_title.lower() for i in range(len(artists))]):  # artist name in video title AND track name in video title
        correct_song += 1024
    elif any([keyword.lower() in video_title.lower() for keyword in
              bad_video_title_keywords]):  # penalize SEVERELY if it's got a "bad keyword"
        correct_song = 0
    else:
        pass

    # Duration
    print("Durations: {v}, {t}".format(v=video_duration, t=track_duration))
    if (-5.0 <= video_duration - (track_duration/1000.0) <= 5.0) or (0.92 <= video_duration / (track_duration/1000.0) <= 1.08):
        duration = 1
    else:
        duration = 0

    print("Subscores: {i}, {c}, {d}".format(i=integrity, c=correct_song, d=duration))
    return integrity * correct_song * duration


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('track_uri', help='Spotify track URI')
    # parser.add_argument('--max-results', help='Max results', default=15)
    args = parser.parse_args()

    try:
        # authenticate
        yt = utils_youtube.authenticate_youtube_key()
        sp = utils_spotify.authenticate_spotify_api_default()

        # Spotify track info
        track_uri = args.track_uri
        # track = sp.track(track_uri)
        artists, track_name, track_duration = utils_spotify.spotify_extract_from_track(sp, track_uri)

        # Conduct yt search query
        vids = utils_youtube.youtube_search_videos(yt, query='"{artist}" "{track}"'.format(
            artist=artists[0], track=track_name),
                                                   max_results=15)

        print(vids)
        print("Length: " + str(len(vids)))

        # Get durations of each video in search
        ids = [entry['id']['videoId'] for entry in vids]
        durations = utils_youtube.youtube_video_durations(yt, ids)

        # Score the results of the query
        vid_scores = [score_video(vids[i], durations[i], artists, track_name, track_duration, i) for i in range(len(vids))]

        print(vid_scores)

        if max(vid_scores) == 0:
            print("I couldn't find a good enough video!")
            quit()

        # find maximum scoring video (if there are multiple, get the first)
        # index_best = max(range(len(vid_scores)), key=)
        index_best = vid_scores.index(max(vid_scores))

        best_video_id = vids[index_best]['id']['videoId']
        best_channel_title = vids[index_best]['snippet']['channelTitle']

        # download best-scoring video
        utils_youtube.download_video(best_video_id, track_id=track_uri, channel_title=best_channel_title)

    except HttpError as e:
        print('An HTTP error {status} occurred:\n{content}'.format(status=e.resp.status, content=e.content))




