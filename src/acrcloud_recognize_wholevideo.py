#!/usr/bin/env python
#-*- coding:utf-8 -*-

import argparse
import json
from datetime import datetime

# import threading
from acrcloud.recognizer import ACRCloudRecognizer

import utils_acrcloud

# This script uses the ACRCloud API to detect music in a video or audio segment using a sliding window method.
# It outputs window times and the actual recognizer result to a json file.

# Before running, make sure you have the ACRCloud package installed.

if __name__ == '__main__':
    # parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("audio", help="name of or path to audio file to analyze")
    parser.add_argument("--shift", type=int,
                        help="how much time to shift between successive audio windows, in seconds")
    parser.add_argument("--duration", type=int, help="duration of each audio window, in seconds")
    args = parser.parse_args()

    # Process shift and duration args
    filepath = args.audio
    if args.shift:
        shift_time = args.shift
    else:
        print("Shift time not provided, using default of 3")
        shift_time = 3
    if args.duration:
        sample_duration = args.duration
    else:
        print("Window duration not provided, using default of 12")
        sample_duration = 12

    # "Log in" to the API
    re = utils_acrcloud.authenticate_acrcloud_default()

    # Conduct many queries using sliding window method and stuff them into a dictionary.
    file_length_in_seconds = int(ACRCloudRecognizer.get_duration_ms_by_file(filepath) / 1000)
    queries = list()  # empty list
    for i in range(0, file_length_in_seconds + 1 - sample_duration, shift_time):
        print("----------------Running recognizer on filepath = {fp}, start time = {start}, end time = {end}"
              "----------------"
              .format(fp=filepath, start=i, end=i+sample_duration))
        query_result = re.recognize_by_file(filepath, i, sample_duration)
        # Create a tuple key (start, end) for query_result in queries
        # Stuff it in
        wrapper = {}
        wrapper["start"] = i
        wrapper["end"] = i + sample_duration
        wrapper["duration"] = sample_duration
        wrapper["result"] = json.loads(query_result)
        queries.append(wrapper)
        # print(type(query_result)) # str, not dict
    print("duration_ms=" + str(ACRCloudRecognizer.get_duration_ms_by_file(filepath)))
    now = str(datetime.utcnow())
    # Write to json file
    outjson = open("../data/text/song_recognition_{filename}_{duration}_{datetime}.json".
                   format(filename=filepath.rpartition('/')[2].rpartition('\\')[2],
                          duration=sample_duration, datetime=now), "w")
    outjson.write(json.dumps(queries, sort_keys=False, indent=4, separators=(',', ': ')))
    outjson.close()