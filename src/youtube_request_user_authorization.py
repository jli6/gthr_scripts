# -*- coding: utf-8 -*-

import argparse
import pickle
from datetime import datetime

import utils_youtube

# This script obtains permissions to look at analytics data, such as viewer retention, from a YouTube channel.
# It saves this to authorization to disk as a Python pickle file
# Please note: Once the channel owner has granted permissions, the code generated by Google expires in about 10 minutes.
# A callback server approach (like the one required to create a Spotify playlist using the API) is much better, but
# I haven't implemented that.

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--channel",
                        help="YouTube channel ID, name, or other keyword that helps you remember which YouTuber you "
                             "requested permission from. Used solely in naming the output pickle file. If this option "
                             "is not invoked, channel keyword will default to 'unknown'.")
    args = parser.parse_args()
    if args.channel:
        channel_name = args.channel
    else:
        channel_name = 'unknown'

    # Disable OAuthlib's HTTPs verification when running locally.
    # *DO NOT* leave this option enabled when running in production.
    # os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

    # Get permission from the channel owner
    youtubeAnalytics = utils_youtube.get_service()

    # Save this googleapiclient.discovery.Resource variable
    now = str(datetime.utcnow())
    f = open('../data/binary/youtubeAnalyticsStore_{user}_{datetime}.pckl'.format(user=channel_name, datetime=now), 'wb')
    pickle.dump(youtubeAnalytics, f)
    f.close()
