# edited version of https://github.com/plamere/spotipy/blob/master/examples/audio_features_for_track.py
# shows acoustic features for tracks for the given artist

# This script is similar to spotify_audio_features_for_track.py. Useful for getting your feet wet and testing, but in pretty
# much every other case spotify_audio_features_for_track.py is superior.

# README:
# Before running this script, you need a Spotify account and a Spotify app with a client id and a client secret.
# Create a file called client_credentials.txt and place the client id and client secret in the file in the following format:
# first line is client id
# second line is client secret
# An example is provided as client_credentials_template.txt. Don't use this file directly -- its credentials are invalid.

from __future__ import print_function  # (at top of module)

import json
import sys
import time

import utils_spotify

if __name__ == '__main__':
    # "Log in" to the API
    sp = utils_spotify.authenticate_spotify_api_default()

    # If user has supplied an argument, find audio features for the track(s)
    # corresponding to the argument(s)
    if len(sys.argv) > 1:
        tids = sys.argv[1:]
        print(tids)

        start = time.time()
        features = sp.audio_features(tids)
        delta = time.time() - start
        print(json.dumps(features, indent=4))
        print ("features retrieved in %.2f seconds" % (delta,))
    # Running the script with no argument, find audio features for a few hardcoded tracks
    else:
        tids = [
            '4buV0v09tf9D5vqAZQBlL5', # Chelle Ives - Underliner
            '4buV0v09tf9D5vqAZQBlL5', # Chelle Ives - Underliner
            '2XW4DbS6NddZxRPm5rMCeY', # Drake - God's Plan (well-known song)
            '0Ljhb7JI02hIEMoj3lxRpZ'] # DJ Mayson - I Have

        start = time.time()
        features = sp.audio_features(tids)
        delta = time.time() - start
        print(json.dumps(features, indent=4))
        print ("features retrieved in %.2f seconds" % (delta,))
        # Note that some tracks don't have any audio features yet.
