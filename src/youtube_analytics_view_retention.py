# -*- coding: utf-8 -*-

import argparse
import json
import pickle
from datetime import *

import utils_youtube

# This script gets view retention data for a specific video. It does not request authorization. Use the youtube_request_user_authorization.py
# script to request authorization and feed that pickle file into this script when you actually query for view retention
# data.


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("video", help="YouTube video ID of the video to examine")
    parser.add_argument("--auth",
                        help="pickle file saved by youtube_request_user_authorization.py containing permissions from the channel that owns the "
                             "specified video")
    parser.add_argument("--plot",
                        help="plot view retention. Requires a graphical environment (e.g. won't work on EC2).",
                        action='store_true')
    args = parser.parse_args()

    video_id = args.video
    if args.auth:
        authfile = args.auth
    else:
        authfile = None
    if args.plot:
        plot = True
    else:
        plot = False

    try:  # If we've already obtained permission don't ask for it again
        f = open(authfile, 'rb')
        youtubeAnalytics = pickle.load(f)
        f.close()
    except IOError:  # If we haven't obtained permission yet, get permission
        # Get permission from the channel owner
        youtubeAnalytics = utils_youtube.get_service()

        # Save this permission variable (type googleapiclient.discovery.Resource)
        now = str(datetime.utcnow())
        f = open('../data/binary/youtubeAnalyticsStore_{vidid}_{datetime}.pckl'.format(vidid=video_id, datetime=now), 'wb')
        pickle.dump(youtubeAnalytics, f)
        f.close()

        # # Save a backup of this (when working with multiple people and their accounts)
        # fbackup = open('youtubeAnalyticsStore_' + time.time() + '.pckl', 'wb')
        # pickle.dump(youtubeAnalytics, fbackup)
        # fbackup.close()

    # Basic request that should work because Google provided it!
    # execute_api_request(
    #     youtubeAnalytics.reports().query,
    #     ids='channel==MINE',
    #     startDate='2017-01-01',
    #     endDate='2017-12-31',
    #     metrics='estimatedMinutesWatched,views,likes,subscribersGained',
    #     dimensions='day',
    #     sort='day'
    # )

    # Request that gets view retention for a specific hardcoded video
    # response_view_retention = execute_api_request(
    #     youtubeAnalytics.reports().query,
    #     ids='channel==UCUcCRNMJ8tEjgWO8J2bzQ4A',
    #     startDate='2016-01-01',
    #     endDate='2018-06-08',
    #     metrics='audienceWatchRatio',
    #     dimensions='elapsedVideoTimeRatio',
    #     filters='video==0zsBaJSWza4;audienceType==ORGANIC'
    # )

    # Request that gets two metrics of view retention for a specific hardcoded video
    # response_view_retention = execute_api_request(
    #     youtubeAnalytics.reports().query,
    #     ids='channel==UCUcCRNMJ8tEjgWO8J2bzQ4A',
    #     startDate='2016-01-01',
    #     endDate='2018-06-08',
    #     metrics='audienceWatchRatio,relativeRetentionPerformance',
    #     dimensions='elapsedVideoTimeRatio',
    #     filters='video==0zsBaJSWza4;audienceType==ORGANIC'
    # )

    # Massey's video, testing new call
    # video_id = 'qAdlq4HPxd0'
    response_view_retention = utils_youtube.execute_view_retention_request(youtubeAnalytics.reports().query, video_id)
    response_views = utils_youtube.execute_views_request(youtubeAnalytics.reports().query, video_id)


    # print(response_view_retention)
    # print(json.dumps(response_view_retention))

    # print(response_views)
    # print(json.dumps(response_views))

    # Convert our dict into json str output. Also pretty prints. Does sort_keys=False work?
    # response_pretty = json.dumps(response_view_retention, sort_keys=False, indent=4, separators=(',', ': '))

    # Convert to native python format -- NOT necessary because execute_api_request gives us a dict!
    # dict_view_retention = json.loads(response_pretty, parse_float='float')
    # dict_view_retention = json.loads(response_pretty)

    # Write data to a file locally
    now = str(datetime.utcnow())
    outjson = open("../data/text/view_retention_{vidid}_{datetime}.json".format(vidid=video_id, datetime=now), "w")
    # outjson.write(json.dumps(response_view_retention))
    outjson.write(json.dumps(response_view_retention, sort_keys=False, indent=4, separators=(',', ': ')))
    outjson.close()

    # print(response_view_retention.get(u'rows'))

    # Plot the view retention graph
    # plt.plot([datapoint[0] for datapoint in response_view_retention.get(u'rows')],
    #          [datapoint[1] for datapoint in response_view_retention.get(u'rows')])
    # plt.xlabel(response_view_retention.get(u'columnHeaders')[0].get(u'name'))
    # plt.ylabel(response_view_retention.get(u'columnHeaders')[1].get(u'name'))
    # plt.title('View retention graph for video')
    # # plt.legend()
    # plt.grid(True)
    # plt.show()

    if plot:
        import matplotlib.pyplot as plt  # putting it here because it's expensive
        # Plot of views rather than ratio
        plt.plot([datapoint[0] for datapoint in response_view_retention.get(u'rows')],
                 [(response_views.get(u'rows')[0][0] * datapoint[1]) for datapoint in response_view_retention.get(u'rows')])
        plt.plot([datapoint[0] for datapoint in response_view_retention.get(u'rows')],
                 [response_views.get(u'rows')[0][0] for datapoint in response_view_retention.get(u'rows')])
        plt.xlabel(response_view_retention.get(u'columnHeaders')[0].get(u'name'))
        plt.ylabel('number of viewers')
        plt.title('Number of views across time')
        # plt.legend()
        plt.grid(True)
        plt.show()
        # plot(response_view_retention.get(u'rows'))