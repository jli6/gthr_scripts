import argparse

import utils_spotify

import utils_acrcloud


# This script wants to take in a Spotify playlist and create a new Spotify playlist containing only songs not detected
# by a third-party audio content recognition (e.g. ACRCloud, Audible Magic) service. In essence, this is a playlist
# filter.

# NOTE: This script is not ready-to-use! It is mostly complete but can't actually create the Spotify playlist because
# it requires interacting with a callback server and I haven't implemented that stuff yet. However, this script is able
# to print out all the songs that aren't recognized by the audio content recognition service. It's also able to
# download Spotify previews. You may find downloading previews useful.


def authenticate_acr():
    """
    Input: None
    Output: Authentication object for our ACR service

    Example:
    In: None
    Out: Mysterious object

    :return: Authentication object for our ACR service
    """
    return utils_acrcloud.authenticate_acrcloud_default()


def create_spotify_playlist(sp_user, username, playlist_name, playlist_description, song_ids):
    """
    Creates Spotify playlist
    :param sp_user: Authenticated Spotify API authentication object for a (not necessarily developer) user
    :param song_ids: array of track ids to add
    :return:
    """
    # TODO: Implement!
    playlist = sp.user_playlist_create(username, playlist_name, playlist_description)
    pass


if __name__ == '__main__':
    # parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("playlist_uri", help="URI of Spotify playlist, e.g. "
                                             "https://open.spotify.com/user/1nkmgmb9adiarj8t67yhuozxz/playlist/02SgGnmOpwmaEYpMuu5aqo")
    args = parser.parse_args()

    # "Log in" to APIs
    sp = utils_spotify.authenticate_spotify_api_default()
    acr = authenticate_acr()

    # 1. Take in spotify playlist, output a bunch of links
    # 2. Take in a bunch of links, output a bunch of downloaded files. Can try to use python or wget for this.
    # 3. Take in a bunch of audio files, output (via an ACR service) recognition results
    # 4. Take in a bunch of song ids, create a spotify playlist.
    #
    # Note that we need to keep track of song ids during the process.
    #
    # Functionally (Wallach style):
    #
    # Let input be a spotify playlist in the form of the raw query response.
    #
    # Then:
    # (because we want artist name, track name for the downloaded filename):
    # playlist_array.map(x -> (x, x.getSongID(), x.getPreviewLink(), x.getArtistName(), x.getTrackName())
    #               .map(x -> (x[0], x[1], x[2].download(x[1], x[3], x[4]))
    #               .filter(x -> x[2] not in acr service)
    #               .toSpotifyPlaylist
    # We use tuples so the song ids are "remembered".
    #
    # It's possible to do it all in one script, if you can make web requests with python.
    #
    # Requires filesystem i/o, Spotify API, Spotify user permissions (unless it's our account that does it), ACR service API.


    # get playlist
    playlist = utils_spotify.get_playlist(sp, args.playlist_uri)
    playlist_entries = playlist['tracks']['items']
    big_x = playlist_entries  # this is the thing that gets functional'ed on
    num_tracks = playlist['tracks']['total']  # size of big_x

    # .map(x -> (x, x.getSongID(), x.getPreviewLink(), x.getArtistName(), x.getTrackName())
    big_x_temp = [None] * num_tracks
    for i in range(num_tracks):
        big_x_temp[i] = (big_x[i],
                         utils_spotify.get_song_id_2(playlist, i),
                         utils_spotify.get_preview_link_playlist(playlist, i),
                         utils_spotify.get_artist_name(playlist, i),
                         utils_spotify.get_track_name(playlist, i))
    big_x = big_x_temp

    # .map(x -> (x[0], x[1], x[2].download(x[1], x[3], x[4]))
    big_x_temp = [None] * num_tracks
    for i in range(num_tracks):
        big_x_temp[i] = (big_x[i][0],
                         big_x[i][1],
                         utils_spotify.download_audio_file_2(
                             big_x[i][2], big_x[i][1], big_x[i][3], big_x[i][4]))
    big_x = big_x_temp

    # .filter(x -> x[2] not in acr service)
    # TODO: Verify that we've correctly addressed the case where there is no preview file at all! Possibly buggy/unintended behavior!
    # Did this in check_acr, need to make sure it's correct!
    big_x_temp = list()
    for i in range(num_tracks):
        # TODO (not urgent): Refactor check_acr such that check_acr is independent between different ACR services (like authenticate_acr())
        # Note check_acr currently uses acrcloud and lives in utils_acrcloud
        if not utils_acrcloud.check_acr(acr, big_x[i][2]):
            big_x_temp.append(big_x[i])
    big_x = big_x_temp

    # for now just print
    print([x[1] for x in big_x])

    # Create Spotify playlist (for now, do it on Gethr account)
    # TODO: Actually create the playlist


    # get track ids and preview urls
    # num_tracks = playlist['tracks']['total']
    # track_ids_and_preview_urls = [None] * num_tracks
    # for i in range(num_tracks):
    #     track_ids_and_preview_urls[i] = (get_song_id_2(playlist, i), get_preview_link_playlist(playlist, i))
    #
    # # download audio files
    # track_ids_and_filenames = [None] * num_tracks
    # for i in range(num_tracks):
    #     track_ids_and_filenames[i] = \
    #         (track_ids_and_filenames[i][0],
    #          download_audio_file_2(
    #              track_ids_and_preview_urls[i][1],
    #          uuu)
    #          )
