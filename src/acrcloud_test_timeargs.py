#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
    >>> python acrcloud_test_timeargs.py test.mp3 5 10
'''

import sys

from acrcloud.recognizer import ACRCloudRecognizer

import utils_acrcloud

# This file is adapted from https://github.com/acrcloud/acrcloud_sdk_python/blob/master/linux/x86-64/python3/test.py.
# It performs one API query and only looks at a user-specified section of the given audio file.
# Useful for getting your feet wet and testing, but if you want to detect music throughout an entire video-length audio
# clip, check out acrcloud_recognize_wholevideo.py.

if __name__ == '__main__':
    # parse args
    filepath = sys.argv[1]
    try:
        start_time = int(sys.argv[2])
    except (ValueError, IndexError):
        start_time = 0
        print("gethr: second argument must be an integer representing the start time of the portion of audio to analyze, in seconds")
        print("gethr: now setting start time to 0")
    try:
        sample_duration = int(sys.argv[3])
    except (ValueError, IndexError):
        sample_duration = 12
        print("gethr: third argument must be an integer representing the duration of the portion of audio to analyze, in seconds")
        print("gethr: now setting duration to 12")

    # "Log in" to the API
    re = utils_acrcloud.authenticate_acrcloud_default()

    #recognize by file path, and skip 0 seconds from from the beginning of sys.argv[1].
    # print(re.recognize_by_file(sys.argv[1], 0, 10))
    print(re.recognize_by_file(filepath, start_time, sample_duration))
    print("duration_ms=" + str(ACRCloudRecognizer.get_duration_ms_by_file(filepath)))

    buf = open(sys.argv[1], 'rb').read()
    #recognize by file_audio_buffer that read from file path, and skip 0 seconds from from the beginning of sys.argv[1].
    # print(re.recognize_by_filebuffer(buf, 0, 10))

