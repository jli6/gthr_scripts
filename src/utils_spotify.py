import requests

from spotipy.oauth2 import SpotifyClientCredentials
import spotipy
import spotipy.util as util


def import_test():
    print("Successfully run method import_test in utils_spotify.py!")


def authenticate_spotify_api(credentials_filepath):
    """
    Input: Filepath to Spotify API client credentials text file
    Output: Authenticated object for Spotify API

    Example:
    In: ('client_credentials.txt')
    Out: <Spotify object> or quit due to error

    :param credentials_filepath: filepath to Spotify API client credentials text file
    :return: Authenticated object for Spotify API
    """
    # Spotify requires an account to use the API, in particular a Client ID and Client Secret.
    # See https://developer.spotify.com/documentation/general/guides/app-settings/#register-your-app
    # You can provide these credentials in a text file

    # right now the credentials_filepath should be 'client_credentials.txt'
    try:
        f = open(credentials_filepath, 'r')
        f_client_id = f.readline().rstrip()
        f_client_secret = f.readline().rstrip()
        f.close()
    except:
        print(
            "gethr: need credentials in a file called client_credentials.txt in the following format: first line is client"
            "id, second line is client secret. Both are 32-digit hexadecimal numbers."
            "A template is provided under the name client_credentials_template.txt")
        quit()

    client_credentials_manager = SpotifyClientCredentials(
        client_id=f_client_id,
        client_secret=f_client_secret)
    sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)
    sp.trace = True

    return sp


def authenticate_spotify_api_default():
    return authenticate_spotify_api('../data/credentials/spotify/client_credentials.txt')


def authenticate_spotify_user(username):
    """
    Request permission from a Spotify user to modify their playlists. MIGHT NOT WORK!
    :param username:
    :return:
    """
    token = util.prompt_for_user_token(username, scope='playlist-modify-public')

    if token:
        sp = spotipy.Spotify(auth=token)
        sp.trace = False
    else:
        print("Can't get token for", username)
        sp = None
    return sp, username


def get_playlist(sp, uri):
    """
    Input: Web URI of Spotify playlist (e.g. https://open.spotify.com/user/1nkmgmb9adiarj8t67yhuozxz/playlist/02SgGnmOpwmaEYpMuu5aqo)
    Output: Spotify API playlist query response
    Retrieve a playlist from the Spotify API whose web address is the uri.

    Example:
    In: ('https://open.spotify.com/user/1nkmgmb9adiarj8t67yhuozxz/playlist/02SgGnmOpwmaEYpMuu5aqo')
    Out: Spotify API user playlist (too long to put here). But its keys (e.g. via get_playlist(uri).keys() should be:
        dict_keys(['href', 'external_urls', 'collaborative', 'images', 'followers', 'owner', 'name', 'uri',
            'description', 'public', 'type', 'tracks', 'id', 'primary_color', 'snapshot_id'])

    :param uri:
    :return: Spotify API playlist query response
    """
    # TODO: Validate input, i.e. make sure it's a valid URI like https://open.spotify.com/user/1nkmgmb9adiarj8t67yhuozxz/playlist/02SgGnmOpwmaEYpMuu5aqo
    # Get JSON form of playlist
    username = uri.split('/')[4]
    playlist_id = uri.split('/')[6]
    results = sp.user_playlist(username, playlist_id)

    return results


def get_song_id(playlist_entry):
    """
    Input: Entry of a Spotify API playlist (i.e. playlist['tracks']['items'][0])
    Output: Track ID of input entry
    Given an entry of a Spotify playlist query result, return its song ID.

    Example:
    In: Too big to put here, but its keys should be:
        dict_keys(['added_by', 'video_thumbnail', 'added_at', 'track', 'primary_color', 'is_local'])
    Out: '57p8CBvPOxrvyCbn6ttl5r'

    :param playlist_entry: Entry of a Spotify API playlist (i.e. playlist['tracks']['items'])
    :return: Track ID of input entry
    """
    # Note that the playlist query result gives us a dictionary. What we really want are
    # entries of results['tracks']['items'], which is a list.
    # Maybe we should accept integer indices as input, too?

    # num_tracks = results['tracks']['total']
    # tracks = results['tracks']['items']
    # Convenience variables
    # Let's assume, for this function, we are given something equivalent to tracks[i].
    # So:
    return playlist_entry['track']['id']


def get_song_id_2(playlist_response, playlist_index):
    """
    Input: Spotify API playlist object and the index position of the desired track (zero-indexed).
    Output: Track ID of the desired track
    Given a Spotify playlist and an index, return the corresponding song ID.

    Example:
    In 1: Spotify API user playlist (too long to put here). But its keys (e.g. via get_playlist(uri).keys() should be:
        dict_keys(['href', 'external_urls', 'collaborative', 'images', 'followers', 'owner', 'name', 'uri',
            'description', 'public', 'type', 'tracks', 'id', 'primary_color', 'snapshot_id'])
    In 2: 2
    Out: '57p8CBvPOxrvyCbn6ttl5r'

    :param playlist_response: Spotify API playlist object
    :param playlist_index: index position of desired track (zero-indexed)
    :return: Track ID of the desired track
    """
    return playlist_response['tracks']['items'][playlist_index]['track']['id']


def get_preview_link_playlist(playlist_response, playlist_index):
    """
    Input: Spotify API playlist object and the index position of the desired track (zero-indexed).
    Output: Preview URL of the desired track
    Given a Spotify playlist and an index, return the corresponding song preview link.

    Example:
    In 1: Spotify API user playlist (too long to put here). But its keys (e.g. via get_playlist(uri).keys() should be:
        dict_keys(['href', 'external_urls', 'collaborative', 'images', 'followers', 'owner', 'name', 'uri',
            'description', 'public', 'type', 'tracks', 'id', 'primary_color', 'snapshot_id'])
    In 2: 2
    Out: 'https://p.scdn.co/mp3-preview/6ffd345e1780495036cca73b343718052d4cab71?cid=cdce7e38ef2942cd905f3a49a7cea62c'

    :param playlist_response: Spotify API playlist object
    :param playlist_index: index position of desired track (zero-indexed)
    :return: Track preview url of desired track
    """
    # TODO: Handle the case where there is no preview link!
    # Note that it may be the case a preview link doesn't exist. Here is an excerpt from a Spotify API query result:
    # 'name': 'Umma Do Me', 'popularity': 41, 'preview_url': None, 'track': True,
    # The 'preview_url" is of type None.
    preview_url = playlist_response['tracks']['items'][playlist_index]['track']['preview_url']
    if not preview_url:  # For now, just print an info message
        print("INFO (getting preview link): Playlist {playlist_href} index {index} track lacks preview link!".format(
            playlist_href=playlist_response['href'],
            index=playlist_index
        ))
    return preview_url  # will be None if the preview url doesn't exist so no need for special case


# def get_preview_link(song_id):
#     """
#     Given a Spotify song ID, get the corresponding preview link.
#     :param song_id:
#     :return:
#     """
#     pass


def get_artist_name(playlist_response, playlist_index):
    # only the first artist. TODO: Get all artist names?
    return playlist_response['tracks']['items'][playlist_index]['track']['artists'][0]['name']


def get_track_name(playlist_response, playlist_index):
    return playlist_response['tracks']['items'][playlist_index]['track']['name']


def download_audio_file_2(preview_link, song_id, artist_name, track_name):
    """
    Input: Spotify preview URL and three arguments whose values only matter in naming the file
    Output: The file that lives at preview_link with filename of the format preview_<song_id>_<artist_name>_<track_name>.mp3
    Side effect: writes a file to disk
    Given a web URL that points to a file and some metadata, download the file and give it a nice name.
    Note that all arguments other than the very first can be anything you want. They're only used for naming the file.

    Example:
    In 1: 'https://p.scdn.co/mp3-preview/6ffd345e1780495036cca73b343718052d4cab71?cid=cdce7e38ef2942cd905f3a49a7cea62c'
    In 2: '57p8CBvPOxrvyCbn6ttl5r' (but it can be anything you want)
    In 3: 'Skrillex' (but it can be anything you want)
    In 4: 'Would You Ever' (but it can be anything you want)
    Out: 'preview_57p8CBvPOxrvyCbn6ttl5r_Skrillex_Would You Ever.mp3'
    Side effect: Writes a file to disk

    :param preview_link:
    :return: the file's name on disk
    """
    file_extension = "mp3"
    filename = "../data/spotify_previews/preview_{id}_{artist}_{track}.{ext}".format(id=song_id, artist=artist_name, track=track_name,
                                                            ext=file_extension)
    return download_audio_file(preview_link, filename)


def download_audio_file(preview_link, filename):
    """
    Input: Spotify preview URL and desired filename of downloaded file
    Output: the input filename
    Side effect: writes a file to disk

    Example:
    In 1: 'https://p.scdn.co/mp3-preview/6ffd345e1780495036cca73b343718052d4cab71?cid=cdce7e38ef2942cd905f3a49a7cea62c'
    In 2: 'preview_57p8CBvPOxrvyCbn6ttl5r_Skrillex_Would You Ever.mp3'
    Out: 'preview_57p8CBvPOxrvyCbn6ttl5r_Skrillex_Would You Ever.mp3'
    Side effect: writes a file to disk

    :param preview_link:
    :param filename:
    :return:
    """
    if preview_link:  # safeguard against null preview_link
        r = requests.get(preview_link)
        with open(filename, "wb") as f:
            f.write(r.content)
        return filename
    else:
        print("INFO (downloading preview link): Link was None!")
        return None


def uri_to_track_id(uri):
    return uri.split('/')[-1]


def spotify_extract_from_track(spotify, track_id):
    """
    Given an authenticated Spotify resource and a Spotify Track ID, perform a query and return a list of all artist
    names, the track name, and the track duration (in milliseconds).
    :param track_id:
    :return:
    """
    track_info = spotify.track(track_id)
    artists = [track_info['artists'][i]['name'] for i in range(len(track_info['artists']))]
    track_name = track_info['name']
    track_duration = track_info['duration_ms']
    return artists, track_name, track_duration

