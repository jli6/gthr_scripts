import argparse
import datetime
from collections import defaultdict

import utils_spotify

# This script computes start and end times of songs in a user-specified Spotify playlist as if the playlist was played
# start to finish with a 10 second crossfade and no shuffle. For example, if the playlist is 1 hour end-to-end, then
# the last song would have an end time of 1 hour. Another example: If I play the playlist at 4:00 PM and I hear the
# 12th song start at 4:40 PM and end at 4:44 PM, then its start time is 40 minutes and its end time is 44 minutes.

# This script writes to a CSV file, which can be easily converted into a Google Sheet.

if __name__ == '__main__':
    # parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("playlist_uri", help="URI of Spotify playlist, e.g. "
                                             "https://open.spotify.com/user/1nkmgmb9adiarj8t67yhuozxz/playlist/02SgGnmOpwmaEYpMuu5aqo")
    parser.add_argument("outfile", help="path to file to write playlist info to")
    args = parser.parse_args()

    # "Log in" to the API
    sp = utils_spotify.authenticate_spotify_api_default()

    # Get JSON form of playlist
    uri = args.playlist_uri
    results = utils_spotify.get_playlist(sp, uri)

    # Given a Spotify playlist, output the start and end times of the songs relative to the start of the playlist,
    # under the following assumption:
    # Playlist is played in order and not shuffled
    #
    # Additional notes:
    # A song can appear in a playlist more than once.
    # A song can have more than one artist.
    # Time is in milliseconds

    # Structure of start_end_info:
    # start_end_info is a dictionary whose keys are song ids and values are dicts.
    # Each value dict in turn has keys denoting the song's indices in the playlist and
    # values (2-element tuples) containing start and end info.
    # If a playlist contains no duplicate songs, each song's value dict will contain one key and one value.
    # In general the size of the value dict for a particular song id is
    # the number of occurrences of that song in the playlist.

    # Calculate start and end times of all songs in the playlist
    # Storage and data variables
    start_end_info = defaultdict(dict)
    time_counter = 0

    # Convenience (or semantic clarity) variables
    gap = 0  # TODO: Find out silence gap between two Spotify songs in a playlist. Currently using a reasonable value.
    # 10 second crossfade means two songs will move closer together (i.e. overlap) by 10 seconds.
    # Take the midpoint of the crossfade window as the start/end boundary.
    # Visually:
    # This is without crossfade
    # ThisIsTheCurrentSong
    #                     ThisIsTheNextSong
    # This is with a crossfade of 10 seconds (10 characters)
    # ThisIsTheCurrentSong
    #           ---------- (Crossfade window)
    #           ThisIsTheNextSong
    # You can see the end time that makes most sense is either at the end or in the middle of the crossfade window, and
    # the start time that makes most sense is either at the beginning or in the middle of the window.
    # Need to ask Massey if middle or extreme makes more sense.
    # Currently using middle.
    crossfade_time = 10000 / 2
    num_tracks = results['tracks']['total']
    tracks = results['tracks']['items']

    for i in range(num_tracks):  # Analyze each entry in the playlist
        # Convenience variables
        unwrapped_track = tracks[i]['track']
        song_id = unwrapped_track['id']
        track_duration = unwrapped_track['duration_ms']

        track_spacing = track_duration - crossfade_time  # time between start of a track and start of its successor (depends on crossfade time)
        track_start = time_counter
        if i is num_tracks:  # Assuming there is no next track, the end time doesn't depend on crossfade
            track_end = time_counter + track_duration
        else:  # General case
            track_end = time_counter + track_spacing
        start_end_info[song_id][i] = (track_start, track_end)

        time_counter += track_spacing

    # Construct spreadsheet CSV and write to file.
    playlist_info = list()
    playlist_info.append("Playlist index, Track ID, Song Name, Primary Artist, All Artists, Start Time, "
                         "Start Time (human-readable), Preview Link")
    for i in range(num_tracks):
        song_id = tracks[i]['track']['id']
        time_entry = start_end_info[song_id][i]
        start_time = time_entry[0]
        end_time = time_entry[1]
        # This CSV-like output format assumes Spotify track names and artist names don't contain commas.
        # I don't know how valid this assumption is.
        artists = tracks[i]['track']['artists']
        playlist_info.append("{idx}, {id}, {song_name}, {primary_artist}, {artists}, {start}, {start_human}, {preview_url}".format(
            idx=i + 1,  # 1-based indexing, for human eyes
            id=song_id,
            song_name=tracks[i]['track']['name'],
            primary_artist=artists[0]['name'],
            artists=" AND ".join([artists[j]['name'] for j in range(len(artists))]),
            start=start_time,
            start_human=str(datetime.timedelta(milliseconds=start_time)),
            preview_url=tracks[i]['track']['preview_url']
        ))

    with open(args.outfile, 'w') as out_file:
        out_file.write("\n".join(playlist_info) + "\n")

