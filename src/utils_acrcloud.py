import json

from acrcloud.recognizer import ACRCloudRecognizer
from acrcloud.recognizer import ACRCloudRecognizeType


def import_test():
    print("Successfully run method import_test in utils_acrcloud.py!")


def authenticate_acrcloud(credentials_filepath):
    """
    Input: Filepath to text file containing ACRCloud credentials
    Output: Authentication object for ACRCloud service

    Example:
    In: ('../acrcloud/projectinfo.txt')
    Out: <ACRCloud object> or quit due to error

    :param filepath: filepath to text file containing ACRCloud credentials
    :return: Authentication object for ACRCloud service
    """
    # right now the credentials_filepath should be '../acrcloud/projectinfo.txt'

    # Read credentials from file
    try:
        f = open(credentials_filepath, 'r')
        host = f.readline().rstrip()
        access_key = f.readline().rstrip()
        access_secret = f.readline().rstrip()
        f.close()
    except: # FileNotFoundError but anything else?
        print("gethr: need credentials in a file called projectinfo.txt in the following format: first line is host "
              "(e.g. identify-us-west-2.acrcloud.com), second line is access key (a 32-digit hexadecimal number,"
              " third line is access secret (a long alphanumeric string). "
              "A template is provided under the name projectinfo_example.txt")
        quit()

    config = {
        'host': host,
        'access_key': access_key,
        'access_secret': access_secret,
        'recognize_type': ACRCloudRecognizeType.ACR_OPT_REC_BOTH,
        'debug': False,
        'timeout': 10  # seconds
    }

    '''This module can recognize ACRCloud by most of audio/video file. 
            Audio: mp3, wav, m4a, flac, aac, amr, ape, ogg ...
            Video: mp4, mkv, wmv, flv, ts, avi ...'''
    re = ACRCloudRecognizer(config)

    return re


def authenticate_acrcloud_default():
    return authenticate_acrcloud('../data/credentials/acrcloud/projectinfo.txt')


def check_acr(acr, audio_file):
    """
    Input: authenticated acr object and Path to an audio file
    Output: true if the ACR service yields a positive identification for the audio file (whether a true positive or
    false positive, doesn't matter)
    :param audio_file:
    :return:
    """
    # TODO (not important): Rename this function and add a wrapper with the name check_acr that doesn't care about implementation

    if not audio_file:
        print("Input audio_file was None! This probably means downloading the track was unsuccessful.")
        return True  # It could be true, it could be false... but we don't want any false negatives.

    # Since an ACRCloud query has a long wait time, print a message so the user knows the program hasn't frozen.
    print("Running ACRCloud recognition on file {filename}".format(filename=audio_file))

    # this is specific to acrcloud.
    response = acr.recognize_by_file(audio_file, 10, 15)  # this returns a string, not a dict
    result = json.loads(response)
    #print(result)

    # the "no result" result looks like this:
    # {"status":{"msg":"No result","code":1001,"version":"1.0"}}
    # but there can be other failures like exceeding query limit
    # so we use the success code instead, which is 0
    if result['status']['code'] == 0:
        for song in result['metadata']['music']:
            if song['score'] == 100:
                return True
        return False

    return False