import argparse
import json
import logging

# This script tells you what songs are in a video by giving you the corresponding Spotify IDs.
# Due to the nature of audio recognition and sampling, there can be inaccuracies, even if the threshold argument is set
# to 100.
# Also, no ACRCloud API queries are being performed. Rather, it takes in the output of a bunch of queries and extracts
# Spotify track IDs from them.

# Run the following command to read the help page for this script:
# $ python3 acrcloud_to_spotify_ids.py -h


if __name__ == '__main__':
    # Parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("input",
                        help="input JSON file in the same format as a file written by acrcloud_recognize_wholevideo.py")
    # parser.add_argument("output",
    #                     help="output file to write Spotify IDs in")
    parser.add_argument("--threshold", type=int,
                        help="only tracks scoring at or higher than this will be output. Default is 98.")
    args = parser.parse_args()
    if args.threshold is not None:
        threshold = args.threshold
    else:
        threshold = 98

    # Logging code adapted from https://stackoverflow.com/questions/5574702/how-to-print-to-stderr-in-python/41304513#41304513
    # My goal is to ensure error messages are printed and visible, but don't pollute the output when you use shell pipes/redirection.
    # TODO: However, it seems that error messages appear in redirected output anyway...how to fix?
    logging.basicConfig(format='%(message)s')

    # Load input file as JSON
    with open(args.input, 'r') as in_file:
        input_json = json.load(in_file)

    # Process the JSON file
    spotify_ids = set()  # use a set to avoid duplicate song ids
    for window_result in input_json:  # extract track spotify id from each ACRCloud query
        if window_result["result"]["status"]["msg"] == "Success":
            music = window_result["result"]["metadata"]["music"]
            for entry in music:
                if entry["score"] >= threshold:
                    try:
                        spotify_ids.add(entry["external_metadata"]["spotify"]["track"]["id"])
                    except:
                        logging.warning("Music detected, but no corresponding spotify track id available for window from "
                                     "{start} to {end}".format(start=window_result["start"], end=window_result["end"]))

    print("\n".join(list(spotify_ids)))
    # print(" ".join(list(spotify_ids)))
