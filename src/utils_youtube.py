import isodate
import youtube_dl

from datetime import datetime, timedelta, date

import google.oauth2.credentials
import google_auth_oauthlib.flow

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow


def get_service():
    # Choose the scope we want for the API query. Wrong scope gives us no rows and endless frustration!
    # SCOPES = ['https://www.googleapis.com/auth/yt-analytics.readonly']
    # SCOPES = ['https://www.googleapis.com/auth/yt-analytics-monetary.readonly']
    # Changing the scope fixes the problem, see
    # https://productforums.google.com/forum/#!topic/youtube/e08jHBGtA_w
    SCOPES = ['https://www.googleapis.com/auth/youtube.readonly']

    API_SERVICE_NAME = 'youtubeAnalytics'
    API_VERSION = 'v2'
    # CLIENT_SECRETS_FILE = 'YOUR_CLIENT_SECRET_FILE.json'
    CLIENT_SECRETS_FILE = '../data/credentials/youtube/youtube_client_id.json'

    flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRETS_FILE, SCOPES)
    credentials = flow.run_console()
    return build(API_SERVICE_NAME, API_VERSION, credentials=credentials)


def authenticate_youtube_key():
    # Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
    # tab of
    #   https://cloud.google.com/console
    # Please ensure that you have enabled the YouTube Data API for your project.
    with open("../data/credentials/youtube/developer_key") as dev_key:
        DEVELOPER_KEY = dev_key.readline().rstrip()
    # print(DEVELOPER_KEY)
    YOUTUBE_API_SERVICE_NAME = 'youtube'
    YOUTUBE_API_VERSION = 'v3'
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                    developerKey=DEVELOPER_KEY)
    return youtube


def execute_api_request(client_library_function, **kwargs):
    """
    Conduct a query.
    :param client_library_function:
    :param kwargs:
    :return:
    """
    response = client_library_function(
        **kwargs
    ).execute()

    return(response)


def execute_view_retention_request(client_library_function, video_id):
    """
    Do a view retention query. video_id is a string, e.g. 'qAdlq4HPxd0'.
    :param client_library_function:
    :param video_id:
    :return:
    """
    return execute_api_request(client_library_function,
                               ids='channel==MINE',  # channel of the authenticated user
                               startDate='1970-01-01',
                               endDate=date.today().isoformat(),  # today's date
                               metrics='audienceWatchRatio',
                               dimensions='elapsedVideoTimeRatio',
                               filters='video==' + video_id
                               )


def execute_views_request(client_library_function, video_id):
    """
    Get number of total views of a video.
    :param client_library_function:
    :param video_id:
    :return:
    """
    return execute_api_request(client_library_function,
                               ids='channel==MINE',  # channel of the authenticated user
                               startDate='1970-01-01',
                               endDate=date.today().isoformat(),  # today's date
                               metrics='views',
                               filters='video==' + video_id
                               )


def download_video(video_id, track_id='', channel_title=''):
    """
    Download a YouTube video given a video ID and Spotify track ID (optional, used in file naming). Filename will be (track_id)_(video_id)
    :param video_id:
    :return:
    """
    # example terminal command equivalent (filename slightly different):
    # $ youtube-dl -x --output '../gthr/adirectory/%(id)s.%(ext)s' https://www.youtube.com/watch?v=BaW_jenozKc

    class MyLogger(object):
        def debug(self, msg):
            pass

        def warning(self, msg):
            print(msg)

        def error(self, msg):
            print(msg)

    def my_hook(d):
        if d['status'] == 'finished':
            print('Done downloading, now converting ...')

    ydl_opts = {
        'format': 'bestaudio/best',
        'outtmpl': '../data/yt_downloads/{track}_%(id)s_{channel}.%(ext)s'.format(
            track=track_id.split('/')[-1],
            channel=channel_title),
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            # 'preferredcodec': 'mp3',
            # 'preferredquality': '192',
        }],
        'logger': MyLogger(),
        'progress_hooks': [my_hook],
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download(['https://www.youtube.com/watch?v={id}'.format(id=video_id)])


def youtube_search_videos(youtube, query, max_results):
    """
    Conduct a YouTube API search query and return an array of all videos obtained.
    :param youtube: authenticated YouTube object
    :param query:
    :param max_results:
    :return:
    """

    # Call the search.list method to retrieve results matching the specified
    # query term.
    # print(query)
    search_response = youtube.search().list(
        q=query,
        part='id,snippet',
        maxResults=max_results
    ).execute()

    # print(search_response)

    # Extract all videos. Note that the structure of what's returned is probably different than the search_response
    # object itself.
    videos = []
    for search_result in search_response.get('items', []):
        if search_result['id']['kind'] == 'youtube#video':
            videos.append(search_result)

    return videos


def youtube_video_duration(youtube, id):
    """
    In: yt vid id.
    Out: decimal number representing duration in seconds
    :param youtube:
    :param id:
    :return:
    """
    video_response = youtube.videos().list(id=id, part='contentDetails').execute()
    duration_string = video_response['items'][0]['contentDetails']['duration']
    return isodate.parse_duration(duration_string).total_seconds()


def youtube_video_durations(youtube, ids):
    # array for now rather than proper varargs
    id_str = ', '.join(ids)
    video_response = youtube.videos().list(id=id_str, part='contentDetails').execute()
    durations = [item['contentDetails']['duration'] for item in video_response['items']]
    seconds = [isodate.parse_duration(dur).total_seconds() for dur in durations]
    return seconds