// Test script for using Liveme API.

/*
 * This script will send one query to the Liveme API (which is most likely unofficial and undocumented) requesting
 * information about a specified video. If you want to send many queries periodically, you will need to edit this script.
 * I advise against just using bash to run this script many times because logging in takes a few seconds and it's a waste
 * to log in for every query.
 */

/*
 * There are two versions of the API. You may find it easy to install the version available in npm, but THAT VERSION
 * DOES NOT WORK! Use the version of the API from Lewdninja on github rather than the one in the npm repo for best
 * results. Here's how to install the Lewdninja version:
 * $ npm install git+https://git@github.com/Lewdninja/liveme-api.git
 * (per https://stackoverflow.com/questions/17509669/how-to-install-an-npm-package-from-github-directly)
 * If this version fails try using Lewdninja's version but an older revision from May 2018:
 * https://github.com/Lewdninja/liveme-api/commit/b1070229607bc2872c85470ad31e941977630f04
 * You can use the below command to do so:
 * $ npm install git+https://git@github.com/Lewdninja/liveme-api.git#b1070229607bc2872c85470ad31e941977630f04
 */

/*
 * Note: Logging in may sometimes fail with an error like "Authentication failed. - TransformError: Error: Request failed."
 * and work other times, even when the code is the same! Just keep trying and trying again.
 * I wasn't able to successfully log in today (2018-08-11) but this script worked in the past (2018-07-15).
 */

/*
 * HOW TO RUN:
 * $ node test.js
 */

const LivemeAPI = require('liveme-api');
const Liveme = new LivemeAPI({
    // TODO: replace these with your actual credentials.
    // DON'T PUSH YOUR CODE IF IT HAS YOUR CREDENTIALS IN IT!!!
    email: 'email_goes_here@example.com',
    password: 'password_goes_here'
});
// console.log('logged in');

// TODO (optional, but highly desirable): Wait for the constructor to finish rather than using a hardcoded wait time
setTimeout(function(){
    // TODO: Replace the getVideoInfo input string below with the id of the video (livestream) you want
    // You can get the id by looking at the URL of a stream you're watching:
    // https://www.liveme.com/us/v/15316515211301476822/index.html
    // To be clear it's the number after /v/ and before /index.html.
	Liveme.getVideoInfo('15316515211301476822')
	    .then(vid => {
	        // user.user_info contains details on the user queried
		console.log('success');
		console.log(vid);
	    })
	    .catch(err => {
	        // Unable to locate user account
		console.log('failed');
	    });
}, 6000);
