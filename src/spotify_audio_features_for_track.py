# edited version of https://github.com/plamere/spotipy/blob/master/examples/audio_features_for_track.py

# This script shows Spotify audio features for user-specified tracks.
# View the help page for this script:
# $ python3 audio_features_for_track -h

# README:
# Before running this script, you need a Spotify account and a Spotify app with a client id and a client secret.
# Create a file called client_credentials.txt and place the client id and client secret in the file in the following format:
# first line is client id
# second line is client secret
# An example is provided as client_credentials_template.txt. Don't use this file directly -- its credentials are invalid.

# This script takes in a file of track IDs (separated by newlines) as input!
# Update 2018-08-14: Please note the audio_features() function provided by spotipy, used here, has a limit of 50 track IDs.

from __future__ import print_function  # (at top of module)

import argparse
import json
import time

import utils_spotify

if __name__ == '__main__':
    # parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("audio", help="text file containing Spotify song URIs, separated by newlines")
    parser.add_argument("outfile", help="output file to write query results (audio features of each track)")
    args = parser.parse_args()

    # "Log in" to the API
    sp = utils_spotify.authenticate_spotify_api_default()

    # Read file of track IDs
    with open(args.audio) as ids_file:
        tids = ids_file.readlines()

    # Process track IDs
    for i in range(len(tids)):
        tids[i] = tids[i].rstrip()
    # print(tids)

    # Query API for audio features, save results to disk, and print the time it took to complete the query (not necessary).
    start = time.time()
    features = sp.audio_features(tids)
    delta = time.time() - start
    with open(args.outfile, "w") as out_file:
        json.dump(features, out_file, indent=4)
    # print(json.dumps(features, indent=4))
    print ("features retrieved in %.2f seconds" % (delta,))
