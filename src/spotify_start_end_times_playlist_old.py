import argparse
import datetime
from collections import defaultdict

import utils_spotify

# This script computes start and end times of songs in a user-specified Spotify playlist as if the playlist was played
# start to finish with no shuffle. For example, if the playlist is 1 hour end-to-end, then the last song would have an
# end time of 1 hour. Another example: If I play the playlist at 4:00 PM and I hear the 12th song start at 4:40 PM and
# end at 4:44 PM, then its start time is 40 minutes and its end time is 44 minutes.
# You may find this script useful for getting your feet wet and testing. However, the spotify_start_end_times_playlist_to_file.py script is
# vastly superior.

if __name__ == '__main__':
    # parse args
    parser = argparse.ArgumentParser()
    parser.add_argument("playlist_uri", help="URI of Spotify playlist, e.g. "
                                             "https://open.spotify.com/user/1nkmgmb9adiarj8t67yhuozxz/playlist/02SgGnmOpwmaEYpMuu5aqo")
    # parser.add_argument("song_ids", help="text file containing Spotify song URIs, separated by newlines")
    args = parser.parse_args()

    # "Log in" to the API
    sp = utils_spotify.authenticate_spotify_api_default()

    # Query API for playlist
    uri = args.playlist_uri
    results = utils_spotify.get_playlist(sp, uri)


    # meat is in results['tracks']
    # number of tracks is in results['tracks']['total']
    # list of tracks is in results['tracks']['items']
    # the first track is results['tracks']['items'][0]
    # though really it's results['tracks']['items'][0]['track']
    # but then we have to deal with the same wrapping paper for this again???
    # Duration of first track: results['tracks']['items'][0]['track']['duration_ms']
    # results['tracks']['items'][0]['track']['track'] is a f__ing boolean and we don't really care about it

    # But your first priority is a script that can take in a spotify playlist
    # and a
    # single or list of unique song IDs and out put the start and end times of those
    # songs relative to the start of the playlist if playlist was played in order and
    # not shuffled

    # Observe: A song can appear in a playlist more than once.
    # So I'm imagining storing results as follows:
    # Dictionary where the key is a song id
    # value is another dict, because of our observation
    # which has key 0 through num_tracks (i.e. its position in the playlist)
    # and value a start and end data (can use a tuple for this).
    # in most cases, the inner dict will have one key. in the exceptional case alluded to
    # by the observation, it'll have more than one.

    start_end_info = defaultdict(dict)
    time_counter = 0  # in ms
    gap = 0  # TODO: Find out the silence gap between two Spotify songs in a playlist. Currently using a reasonable placeholder value.

    # convenience variables
    num_tracks = results['tracks']['total']
    tracks = results['tracks']['items']
    # array of len num_tracks that stores the start and end times
    for i in range(num_tracks):
        unwrapped_track = tracks[i]['track']
        song_id = unwrapped_track['id']
        track_duration = unwrapped_track['duration_ms'] - 5000
        track_start = time_counter
        if i is num_tracks:
            track_end = time_counter + track_duration + 5000
        else:
            track_end = time_counter + track_duration
        start_end_info[song_id][i] = (track_start, track_end)

        time_counter += track_duration


        # print(track_duration)

    # print(start_end_info)

    # Read the song_ids input file
    # with open(args.song_ids) as ids_file:
    #     tids = ids_file.readlines()
    # # Get the pure song ids
    # for i in range(len(tids)):
    #     tids[i] = tids[i].rstrip().split('/')[-1]  # should work with both URIs and raw ids

    tids = [tracks[i]['track']['id'] for i in range(num_tracks)]
    print(tids)
    # Do the checking. For now, if a song appears multiple times, gives an arbitrary result.
    print("************ BEGIN RESULTS ************\n")
    for i in range(len(tids)):
        tid = tids[i]
        if tid in start_end_info:
            time_entry = next(iter(start_end_info[tid].values()))
            start_time = time_entry[0]
            end_time = time_entry[1]

            # print(next(iter(start_end_info[tid].values())))
            # print("******** RESULT ********\n"
            #       "Track: {id}\n"
            #       "Start time in playlist: {start} ms ({starth})\n"
            #       "End time in playlist: {end} ms ({endh})\n\n"
            #       .format(id=tid,
            #               start=start_time,
            #               end=end_time,
            #               starth=str(datetime.timedelta(milliseconds=start_time)),
            #               endh=str(datetime.timedelta(milliseconds=end_time))
            #               ))
            print("{id}, {songname}, {artist}, {start}, {starth}".format(
                id=tid,
                songname=tracks[i]['track']['name'],
                artist=tracks[i]['track']['artists'][0]['name'], # TODO: Support multiple artists
                start=start_time,
                starth=str(datetime.timedelta(milliseconds=start_time)),
            ))
        else:
            print("******** RESULT ********\n"
                  "Track {id} not in playlist!\n\n".format(id=tid))


    # test_id = "4XisomkfrfzGKl0iPD6Ox6"
    # if test_id in start_end_info:
    #     print(start_end_info[test_id])
    # else:
    #     print("not in playlist")

    # print(json.dumps(results, indent=4))