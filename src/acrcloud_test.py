#!/usr/bin/env python
#-*- coding:utf-8 -*-

'''
    >>> python acrcloud_test.py test.mp3
'''

import sys

from acrcloud.recognizer import ACRCloudRecognizer

import utils_acrcloud

# This file is adapted from https://github.com/acrcloud/acrcloud_sdk_python/blob/master/linux/x86-64/python3/test.py.
# It performs one API query and only looks at the first 10 seconds of the given audio file.
# Useful for getting your feet wet and testing, but if you want to detect music throughout an entire video-length audio
# clip, check out acrcloud_recognize_wholevideo.py.

if __name__ == '__main__':
    # "Log in" to the API
    re = utils_acrcloud.authenticate_acrcloud_default()

    # recognize by file path, and skip 0 seconds from from the beginning of sys.argv[1].
    print(re.recognize_by_file(sys.argv[1], 0, 10))

    print("duration_ms=" + str(ACRCloudRecognizer.get_duration_ms_by_file(sys.argv[1])))

    buf = open(sys.argv[1], 'rb').read()
    # recognize by file_audio_buffer that read from file path, and skip 0 seconds from from the beginning of sys.argv[1].
    print(re.recognize_by_filebuffer(buf, 0, 10))

